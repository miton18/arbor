# Copyright 2021 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

REPO=linux/kernel/git/jejb/${PN}.git

require kernel.org [ repo=${REPO} ]

SUMMARY="Useful tools for manipulating UEFI secure boot platforms"
DOWNLOADS="https://git.kernel.org/pub/scm/${REPO}/snapshot/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:=
        dev-perl/File-Slurp
        sys-apps/help2man
    build+run:
        sys-boot/gnu-efi
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PNV}-makefile.patch )

src_prepare(){
    default

    edo sed -e "/OBJCOPY\s\+=\s/s:objcopy:$(exhost --tool-prefix)objcopy:" \
            -e "/BINDIR\s\+=\s/s:/usr/bin:/usr/$(exhost --target)/bin:" \
            -e "/^\s\+ar\s/s:ar:$(exhost --tool-prefix)ar:" \
            -i Make.rules
}

