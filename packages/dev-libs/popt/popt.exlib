# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require toolchain-funcs

export_exlib_phases src_prepare src_install

SUMMARY="Command line parsing library"
HOMEPAGE="https://github.com/rpm-software-management/popt"
DOWNLOADS="http://ftp.rpm.org/${PN}/releases/${PN}-1.x/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    parts: development documentation libraries
    ( linguas: cs da de eo es fi fr ga gl hu id is it ja ko lv nb nl pl pt ro ru sk sl sv th tr uk
               vi wa zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-doxygen
    --disable-static
    --disable-werror
)

popt_src_prepare() {
    default

    if ld-is-gold || ld-is-lld; then
        # The executable name in `test1 --usage` is
        # 'lt-test1' when using bfd
        # 'test1' when using gold or lld
        edo sed -e 's:lt-test1:test1:' -i tests/testit.sh
    fi
}

popt_src_install() {
    default

    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
    expart development /usr/$(exhost --target)/{include,lib/pkgconfig}
}

