# Copyright 2019-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=pybind tag=v${PV} ] cmake \
    setup-py [ import=setuptools blacklist=none ]

SUMMARY="Lightweight header-only library that exposes C++ types in Python"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# broken, last checked: 2.6.2
RESTRICT="test"

DEPENDENCIES="
    test:
        dev-python/pytest[python_abis:*(-)?]
"

src_unpack() {
    cmake_src_unpack
    easy-multibuild_src_unpack
}

src_prepare() {
    cmake_src_prepare
    setup-py_src_prepare
}

src_configure() {
    local cmakeparams=(
        -DBUILD_TESTING:BOOL=$(expecting_tests TRUE FALSE)
        -DCMAKE_BUILD_TYPE:STRING=Release
        -DCMAKE_DISABLE_FIND_PACKAGE_Boost:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_Catch:BOOL=TRUE
        -DCMAKE_DISABLE_FIND_PACKAGE_Eigen3:BOOL=TRUE
        -DPYBIND11_INSTALL:BOOL=TRUE
        -DPYBIND11_NOPYTHON:BOOL=FALSE
        -DPYBIND11_WERROR:BOOL=FALSE
        -DPYTHON_EXECUTABLE:PATH=${PYTHON}
    )

    ecmake \
        "${cmakeparams[@]}"

    setup-py_src_configure
}

src_compile() {
    emake
    setup-py_src_compile
}

src_test() {
    emake check
}

src_install() {
    cmake_src_install
    setup-py_src_install
}

